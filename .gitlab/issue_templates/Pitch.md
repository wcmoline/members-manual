### Problem
>*Describe the problem, opportunity, or use case that you're thinking about.*

>*Establishing the problem also lets us have a clearer conversation later when it’s time to pitch the idea or bet on it. We want to be able to separate out the discussion about the demand so we don’t spend time on a good solution that doesn’t address a valuable problem.*

>*How far you have to go to spell out the problem will depend on how much context you share with the people reading the write-up. The best problem definition consists of a single specific story that shows why the status quo doesn’t work. This gives you a baseline to test fitness against. People will be able to weigh the solution against this specific problem—or other solutions if a debate ensues—and judge whether or not that story has a better outcome with the new solution swapped in.*

Write your answers here: 

### What's important about this problem?
>*What should we keep in mind as we consider solutions to this problem?*

Write your answers here:

### Constraints
>*Stating the constraints in the pitch prevents unproductive conversations. There’s always a better solution. The question is, if we only care enough to spend two weeks on this now, how does this specific solution look?*

>*Anybody can suggest expensive and complicated solutions. It takes work and insight to get to a simple idea that fits in a small-time box. Stating constraints and embracing them turns everyone into a partner in that process.*

Write your answers here:

### Solution
>*A problem without a solution is unshaped work. Giving it to a team means pushing research and exploration down to the wrong level, where the skillsets, time limit, and risk profile (thin vs. heavy-tailed) are all misaligned.*

>*If the solution isn’t there, someone should go back and do the shaping work on the shaping track. It’s only ready to bet on when problems, constraints, and solutions come together. Then you can scrutinize the fit between problem and solution and judge whether it’s a good bet or not.*

Write your answers here:
